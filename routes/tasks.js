var Model = require('./../models/Task');

/* Save a user */
var saveTask = function (req, res) {
	new Model.Task({
		title: req.body.title,
		description: req.body.description,
		is_complete: req.body.is_complete,
		user_id: req.body.user_id,
	}).save()
		.then(function (task) {
			res.json(task);
		}).catch(function (error) {
			console.log(error);
			res.send('An error occured');
		});
};

/* Get all users */
var getAllTasks = function (req, res) {
	new Model.Task().fetchAll()
		.then(function (tasks) {
			res.json(tasks);
		}).catch(function (error) {
			console.log(error);
			res.send('An error occured');
		});
};

/* Delete a user */
var deleteTask = function (req, res) {
	var taskId = req.params.id;
	new Model.Task().where('id', taskId)
		.destroy()
		.catch(function (error) {
			console.log(error);
			res.send('An error occured');
		});
};

/* Get a user */
var getTask = function (req, res) {
	var taskId = req.params.id;
	new Model.Task().where('id', taskId)
		.fetch()
		.then(function (task) {
			res.json(task);
		}).catch(function (error) {
			console.log(error);
			res.send('An error occured');
		});
};

/* Exports all methods */
module.exports = {
	saveTask: saveTask,
	getAllTasks: getAllTasks,
	deleteTask: deleteTask,
	getTask: getTask
};
