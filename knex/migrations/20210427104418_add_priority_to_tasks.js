'use strict';

exports.up = function(knex, Promise) {
  return knex.schema.table('tasks', function(table) {
    table.integer('priority').nullable()
  })
};

exports.down = function(knex, Promise) {
  return knex.schema.table('tasks', function(table) {
    table.dropColumn('priority')
  })
};
