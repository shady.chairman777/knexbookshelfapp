const database = require("./knex");

module.exports = {
    list(){
        return database('tasks');
    },
    read(id){
        return database('tasks').where('id', id).returning('*')
            .then(record => record[0])
    },
    create(table_object){
        return database('tasks').insert(table_object).returning('*')
            .then(record => record[0])
    },
    update(id, table_object){
        return database('tasks').update(table_object).where('id', id).returning('*');
            //.then(record => record[0])  
            //Do I need the above when the same thing is basically in the router.put?
    },
    delete(id){
        return database('tasks').del().where('id', id);
    }
};