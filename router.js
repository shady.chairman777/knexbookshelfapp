var user = require('./routes/users');
var index = require('./routes/index');
var task = require('./routes/tasks');
//var task = require("./knex/queries");

module.exports = function (app) {

	/* Index(main) route */
	app.get('/', index.index);

	/* User Routes */
	app.post('/users', user.saveUser);
	app.get('/users', user.getAllUsers);
	app.delete('/user/:id', user.deleteUser);
	app.get('/user/:id', user.getUser);
	
	/* User Routes */
	app.post('/tasks', task.saveTask);
	app.get('/tasks', task.getAllTasks);
	app.delete('/task/:id', task.deleteTask);
	app.get('/task/:id', task.getTask);
};

