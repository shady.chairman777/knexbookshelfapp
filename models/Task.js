var bookshelf = require('./../config/db').bookshelf;

var Task = bookshelf.Model.extend({
	tableName: 'tasks'
});

module.exports = {
	Task: Task
};
